// Import libraries
import React from "react";
import { View, StyleSheet } from "react-native";

// Import constants
import { COLOR } from "constants/color";
import { GOOGLE_ICON } from "constants/image";

// Import components
import CustomTextInput from "components/form/CustomTextInput";
import Label from "components/form/Label";
import CustomButton from "components/form/CustomButton";
import Title from "components/shared/Title";
import CustomIconButton from "components/form/CustomIconButton";
import RootStackParamList from "types/screen";
import { NativeStackScreenProps } from "@react-navigation/native-stack";

type Props = NativeStackScreenProps<RootStackParamList, "Signup">;

const SignUpScreen = ({} : Props) => {
  return (
    <View style={styles.container}>
      <Title />
      <View
        style={{
          width: "80%",
          padding: 20,
          backgroundColor: COLOR.WHITE,
          borderRadius: 10,
          display: "flex",
          gap: 10,
        }}
      >
        <View style={{ display: "flex", gap: 2 }}>
          <Label text="Email" />
          <CustomTextInput placeholder="Your Email" />
        </View>
        <View style={{ display: "flex", gap: 2 }}>
          <Label text="Password" />
          <CustomTextInput secureTextEntry placeholder="*******" />
        </View>
        <View style={{ display: "flex", gap: 2 }}>
          <Label text="Repeat Password" />
          <CustomTextInput secureTextEntry placeholder="*******" />
        </View>
        <CustomButton text="Sign Up" />
        <CustomIconButton icon={GOOGLE_ICON} text="Sign Up With Google" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.BACKGROUND_COLOR,
    alignItems: "center",
    justifyContent: "center",
    gap: 40,
  },
});

export default SignUpScreen;
