// Import libraries
import React from "react";
import { View, StyleSheet } from "react-native";

// Import constants
import { COLOR } from "constants/color";

// Import components
import CustomTextInput from "components/form/CustomTextInput";
import Label from "components/form/Label";
import CustomButton from "components/form/CustomButton";
import Title from "components/shared/Title";

// Import constants
import { NativeStackScreenProps } from "@react-navigation/native-stack";

// Import types
import RootStackParamList from "types/screen";

// Tooltip
// ->The UI Component (In shared folder)
// ->Provider
// Password checking
// Make the coloured bar on top to show strength of password
// Make Login Screen
// Get google icon
// To different component
// Keyboard avoiding view

type Props = NativeStackScreenProps<RootStackParamList, "Login">;

const LoginScreen = ({}: Props) => {
  return (
    <View style={styles.container}>
      <Title />
      <View
        style={{
          width: "80%",
          padding: 20,
          backgroundColor: COLOR.WHITE,
          borderRadius: 10,
          display: "flex",
          gap: 10,
        }}
      >
        <View style={{ display: "flex", gap: 2 }}>
          <Label text="Email" />
          <CustomTextInput placeholder="Your Email" />
        </View>
        <View style={{ display: "flex", gap: 2 }}>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              gap: 5,
            }}
          >
            <Label text="Password" />
          </View>
          <CustomTextInput secureTextEntry placeholder="*******" />
        </View>
        <CustomButton text="Login" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.BACKGROUND_COLOR,
    alignItems: "center",
    justifyContent: "center",
    gap: 40,
  },
});

export default LoginScreen;
