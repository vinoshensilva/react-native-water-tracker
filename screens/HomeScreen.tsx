// Import libraries
import React from "react";
import { View, StyleSheet } from "react-native";
import { StatusBar } from "expo-status-bar";
import { NativeStackScreenProps } from "@react-navigation/native-stack";

// Import constants
import { COLOR } from "constants/color";

// Import types
import RootStackParamList from "types/screen";

// Import components
import UserProfile from "components/home/UserProfile";

type Props = NativeStackScreenProps<RootStackParamList, "Home">;

const HomeScreen = ({}: Props) => {
  return (
    <View style={styles.container}>
      <UserProfile userName="Vino Silva" dailyGoal={10} />
      <StatusBar style="auto" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.BACKGROUND_COLOR,
    alignItems: "center",
    paddingTop: 20,
    paddingHorizontal: 20,
  },
});

export default HomeScreen;
