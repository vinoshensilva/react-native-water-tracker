// Import libraries
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import * as SplashScreen from "expo-splash-screen";
import { useEffect, useState } from "react";
import { GestureHandlerRootView } from "react-native-gesture-handler";
import { Provider } from "react-redux";

// Import store
import { store } from "store/index";

// Import constants
import RootStackParamList from "types/screen";

// Import screens
import HomeScreen from "screens/HomeScreen";
import LoginScreen from "screens/LoginScreen";
import SignUpScreen from "screens/SignUpScreen";
import { StatusBar, View } from "react-native";

// Import constants
import { COLOR } from "constants/color";

const Stack = createNativeStackNavigator<RootStackParamList>();

// Keep the splash screen visible while we fetch resources
SplashScreen.preventAutoHideAsync();

export default function App() {
  const [appisReady, setIsAppReady] = useState<boolean>();

  useEffect(() => {
    const hideSplashScreen = async () => {
      if (appisReady) {
        await SplashScreen.hideAsync();
      }
    };

    hideSplashScreen();
  }, [appisReady]);

  return (
    <View
      style={{
        paddingTop: StatusBar.currentHeight || 0,
        flex: 1,
        backgroundColor: COLOR.BACKGROUND_COLOR,
      }}
    >
      <Provider store={store}>
        <GestureHandlerRootView style={{ flex: 1 }}>
          <NavigationContainer>
            <Stack.Navigator
              screenOptions={{
                headerShown: false,
              }}
              initialRouteName="Home"
            >
              <Stack.Screen name="Home" component={HomeScreen} />
              <Stack.Screen name="Signup" component={SignUpScreen} />
              <Stack.Screen name="Login" component={LoginScreen} />
            </Stack.Navigator>
          </NavigationContainer>
        </GestureHandlerRootView>
      </Provider>
    </View>
  );
}
