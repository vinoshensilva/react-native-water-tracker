export const COLOR = {
  BACKGROUND_COLOR: "#bbf0eb",
  SECONDARY: "#195053",
  PRIMARY: "#1cb6a9",
  WHITE: "#ebfbfb",
  BLACK: "#282929",
};
