// Import libraries
import React from "react";
import { View, Text, StyleSheet } from "react-native";

// Import components
import Icon from "./Icon";

// Import constants
import { COLOR } from "constants/color";

const Title = () => {
  return (
    <View style={styles.container}>
      <Icon />
      <Text style={styles.text}>Water Tracker</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    gap: 5,
  },
  text: {
    fontSize: 40,
    color: COLOR.SECONDARY,
  },
});

export default Title;
