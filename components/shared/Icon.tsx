// Import libraries
import React from "react";
import { Image } from "react-native";

// Import water icon image
import { WATER_ICON } from "constants/image";

const Icon = () => {
  return (
    <Image
      source={WATER_ICON}
      style={{
        width: 48,
        height: 48,
      }}
    />
  );
};

export default Icon;
