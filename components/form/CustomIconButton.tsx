// Import components and type
import { Image, ImageSourcePropType } from "react-native";
import CustomButton, { CustomButtonProps } from "./CustomButton";
import { COLOR } from "constants/color";

interface CustomIconButtonProps extends CustomButtonProps {
  icon: ImageSourcePropType;
}

const CustomIconButton = ({ icon, ...props }: CustomIconButtonProps) => {
  return (
    <CustomButton
      {...props}
      style={{
        backgroundColor: COLOR.WHITE,
        borderColor: COLOR.BLACK,
        borderWidth: 0.5,
      }}
      textStyle={{
        color: COLOR.BLACK,
      }}
    >
      <Image
        source={icon}
        style={{
          width: 24,
          height: 24,
        }}
      />
    </CustomButton>
  );
};

export default CustomIconButton;
