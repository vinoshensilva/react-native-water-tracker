// Import libraries
import React from "react";
import { TextInput, TextInputProps } from "react-native";

// Import constants
import { COLOR } from "constants/color";

const CustomTextInput = ({style,...props}: TextInputProps) => {
  return (
    <TextInput
      placeholder="michael@gmail.com"
      style={style || {
        borderWidth: 0.25,
        padding: 5,
        borderColor: COLOR.SECONDARY,
        borderRadius: 5,
        color: COLOR.PRIMARY,
      }}
      placeholderTextColor={COLOR.PRIMARY}
      selectionColor={COLOR.PRIMARY}
      {...props}
    />
  );
};

export default CustomTextInput;
