// Import libraries
import { Text } from 'react-native'
import React from 'react'

interface LabelProps {
    text: string;
}

const Label = ({text}: LabelProps) => {
  return (
    <Text>{text}</Text>
  )
}

export default Label