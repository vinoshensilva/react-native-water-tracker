// Import libraries
import React from "react";
import {
  StyleProp,
  StyleSheet,
  Text,
  TextStyle,
  TouchableOpacity,
  TouchableOpacityProps,
  ViewStyle,
} from "react-native";

// Import constants
import { COLOR } from "constants/color";

export interface CustomButtonProps extends TouchableOpacityProps {
  text?: string;
  textStyle?: StyleProp<TextStyle>;
}

const CustomButton = ({
  text = "",
  textStyle,
  style,
  children,
  ...props
}: CustomButtonProps) => {
  return (
    <TouchableOpacity
      style={style ? [styles.button, style] : styles.button}
      {...props}
    >
      {children}
      <Text style={textStyle ? [styles.text, textStyle] : styles.text}>
        {text}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    width: "100%",
    backgroundColor: COLOR.PRIMARY,
    padding: 10,
    borderRadius: 5,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    gap: 10,
  },
  text: {
    color: COLOR.WHITE,
    textAlign: "center",
  },
});

export default CustomButton;
