// Import libraries
import { StyleSheet, Text, View } from "react-native";

// Import constants
import { COLOR } from "constants/color";

interface UserProfileProps {
  userName: string;
  dailyGoal: number;
}

const UserProfile = ({ userName, dailyGoal }: UserProfileProps) => {
  return (
    <View style={styles.container}>
      <Text style={styles.nameText}>{userName}</Text>
      <Text style={styles.dailyGoalText}>
        <Text style={styles.dailyGoalLabel}>Daily Goal:</Text>{" "}
        <Text style={styles.dailyGoalValue}>{`${dailyGoal} Liters`}</Text>{" "}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: COLOR.WHITE,
    borderWidth: 0.5,
    borderColor: COLOR.PRIMARY,
    width: "100%",
    borderRadius: 15,
  },
  nameText: {
    color: COLOR.PRIMARY,
    fontSize: 24,
    fontWeight: "700",
  },
  dailyGoalText: {
    color: COLOR.PRIMARY,
    fontSize: 18,
  },
  dailyGoalLabel: {
    color: COLOR.BLACK,
    fontWeight: "600",
  },
  dailyGoalValue: {
    fontWeight: "700",
  },
});

export default UserProfile;
